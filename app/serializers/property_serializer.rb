class PropertySerializer < ActiveModel::Serializer
  attributes :id, :street_address, :beds, :baths, :rent, :company_id
end
