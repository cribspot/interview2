class StaySerializer < ActiveModel::Serializer
  attributes :id, :start_date, :end_date, :property_id
end
