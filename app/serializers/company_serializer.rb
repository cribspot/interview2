class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :phone
end
