class TenantSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :stay_id
end
