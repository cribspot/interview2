class FurnishingSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :quantity, :notes
  has_one :property
end
