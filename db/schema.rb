# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160519004213) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "furnishings", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "quantity"
    t.text     "notes"
    t.integer  "property_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["property_id"], name: "index_furnishings_on_property_id", using: :btree
  end

  create_table "properties", force: :cascade do |t|
    t.string   "street_address"
    t.integer  "beds"
    t.float    "baths"
    t.integer  "rent"
    t.integer  "company_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["company_id"], name: "index_properties_on_company_id", using: :btree
  end

  create_table "stays", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "property_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["property_id"], name: "index_stays_on_property_id", using: :btree
  end

  create_table "tenants", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.integer  "stay_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stay_id"], name: "index_tenants_on_stay_id", using: :btree
  end

  add_foreign_key "furnishings", "properties"
  add_foreign_key "properties", "companies"
  add_foreign_key "stays", "properties"
  add_foreign_key "tenants", "stays"
end
