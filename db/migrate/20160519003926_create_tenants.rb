class CreateTenants < ActiveRecord::Migration[5.0]
  def change
    create_table :tenants do |t|
      t.string :name
      t.string :email
      t.references :stay, foreign_key: true

      t.timestamps
    end
  end
end
