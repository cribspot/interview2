class CreateFurnishings < ActiveRecord::Migration[5.0]
  def change
    create_table :furnishings do |t|
      t.string :name
      t.text :description
      t.integer :quantity
      t.text :notes
      t.references :property, foreign_key: true

      t.timestamps
    end
  end
end
