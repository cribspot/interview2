class CreateProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :properties do |t|
      t.string :street_address
      t.integer :beds
      t.float :baths
      t.integer :rent
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
