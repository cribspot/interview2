require 'test_helper'

class StaysControllerTest < ActionDispatch::IntegrationTest
  setup do
    @stay = stays(:one)
  end

  test "should get index" do
    get stays_url
    assert_response :success
  end

  test "should create stay" do
    assert_difference('Stay.count') do
      post stays_url, params: { stay: { end_date: @stay.end_date, property_id: @stay.property_id, start_date: @stay.start_date } }
    end

    assert_response 201
  end

  test "should show stay" do
    get stay_url(@stay)
    assert_response :success
  end

  test "should update stay" do
    patch stay_url(@stay), params: { stay: { end_date: @stay.end_date, property_id: @stay.property_id, start_date: @stay.start_date } }
    assert_response 200
  end

  test "should destroy stay" do
    assert_difference('Stay.count', -1) do
      delete stay_url(@stay)
    end

    assert_response 204
  end
end
