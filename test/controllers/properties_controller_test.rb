require 'test_helper'

class PropertiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @property = properties(:one)
  end

  test "should get index" do
    get properties_url
    assert_response :success
  end

  test "should create property" do
    assert_difference('Property.count') do
      post properties_url, params: { property: { baths: @property.baths, beds: @property.beds, company_id: @property.company_id, rent: @property.rent, street_address: @property.street_address } }
    end

    assert_response 201
  end

  test "should show property" do
    get property_url(@property)
    assert_response :success
  end

  test "should update property" do
    patch property_url(@property), params: { property: { baths: @property.baths, beds: @property.beds, company_id: @property.company_id, rent: @property.rent, street_address: @property.street_address } }
    assert_response 200
  end

  test "should destroy property" do
    assert_difference('Property.count', -1) do
      delete property_url(@property)
    end

    assert_response 204
  end
end
