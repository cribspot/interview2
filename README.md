# Cribspot Interview

## REST API
For the rest api, you will have a few different actions. I listed an example below. All endpoints return JSON.

### GET /companies
Gets a list of all the companies.

### GET /companies/1
Gets the company with id of 1

### POST /companies
Creates a new company.

### PATCH/PUT /companies/1
Updates the company with id of 1.

### DELETE /companies/1
Destroys the company with an id of 1.


## Resources

### Company
- *string* name
- *string* phone

### Property
- *string* street_name
- *integer* beds
- *float* baths
- *integer* rent
- *belongs to* company

### Stay
- *date* start_date
- *date* end_date
- *belongs to* property

### Tenants
- *string* name
- *string* email
- *belongs to* stay

### Furnishing
- *string* name
- *text* description 
- *integer* quantity
- *text* notes
- *belongs to* stay
