Rails.application.routes.draw do
  resources :furnishings
  resources :tenants
  resources :stays
  resources :properties
  resources :companies
end
