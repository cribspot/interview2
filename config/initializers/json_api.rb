ActiveModel::Serializer.config.adapter = :json_api

formats = %w(text/x-json application/jsonrequest application/vnd.api+json)
Mime::Type.register "application/json", :json, formats
